@extends('user.layouts.app')
@section('content')
<section class="hero-area" id="home">
    <div class="container">
        <div class="row">
		    <div class="col-lg-7">
                <div class="hero-area-content">
				    <h1>Our Mitra Travel</h1>
				    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
					<a href="#" class="appao-btn">Google Play</a>
					<a href="#" class="appao-btn">App Store</a>
                </div>
			</div>
			<div class="col-lg-5">
                <div class="hand-mockup text-lg-left text-center">
				    <img src="{{asset('assets/img/preview.png')}}" alt="Hand Mockup" />
                </div>
			</div>
        </div>
	</div>
</section>
<section class="team-area ptb-90" id="team">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="sec-title">
					<h2>Daftar Mitra<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                </div>
			</div>
        </div>
        <div class="row">
			<div class="col-lg-3 col-sm-3">
				<div class="single-team-member">
					<div class="team-member-img">
                        <img src="{{asset('assets/img/team/hafiz.jpeg')}}" alt="team">
                        <div class="team-member-icon">
							<div class="display-table">
                                <div class="display-tablecell">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
									<a href="#"><i class="fab fa-instagram"></i></a>
									<a href="#"><i class="fab fa-twitter"></i></a>>
                                </div>
							</div>
                        </div>
					</div>
					<div class="team-member-info">
                        <a href="#"><h4>Hafiz</h4></a>
                        <!-- <p>UX Designer</p> -->
					</div>
                </div>
			</div>
			<div class="col-lg-3 col-sm-3">
				<div class="single-team-member">
					<div class="team-member-img">
                        <img src="{{asset('assets/img/team/anas.jpeg')}}" alt="team">
                        <div class="team-member-icon">
							<div class="display-table">
                                <div class="display-tablecell">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
									<a href="#"><i class="fab fa-instagram"></i></a>
									<a href="#"><i class="fab fa-twitter"></i></a>
                                </div>
							</div>
                        </div>
					</div>
					<div class="team-member-info">
                        <a href="#"><h4>Anas</h4></a>
                        <!-- <p>UX Designer</p> -->
					</div>
                </div>
			</div>
			<div class="col-lg-3 col-sm-3">
				<div class="single-team-member">
					<div class="team-member-img">
                        <img src="{{asset('assets/img/team/ihul.jpeg')}}" alt="team">
                        <div class="team-member-icon">
							<div class="display-table">
                                <div class="display-tablecell">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
									<a href="#"><i class="fab fa-instagram"></i></a>
									<a href="#"><i class="fab fa-twitter"></i></a>
                                </div>
							</div>
                        </div>
					</div>
					<div class="team-member-info">
                        <a href="#"><h4>Ihul</h4></a>
                        <!-- <p>UX Designer</p> -->
					</div>
                </div>
			</div>
            <div class="col-lg-3 col-sm-3">
				<div class="single-team-member">
					<div class="team-member-img">
                        <img src="{{asset('assets/img/team/ihul.jpeg')}}" alt="team">
                        <div class="team-member-icon">
							<div class="display-table">
                                <div class="display-tablecell">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
									<a href="#"><i class="fab fa-instagram"></i></a>
									<a href="#"><i class="fab fa-twitter"></i></a>
                                </div>
							</div>
                        </div>
					</div>
					<div class="team-member-info">
                        <a href="#"><h4>Ihul</h4></a>
                        <!-- <p>UX Designer</p> -->
					</div>
                </div>
			</div>
        </div>
	</div>
</section>
<!-- <section class="blog-area ptb-90" id="blog">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="sec-title">
					<h2>Our Latest Blog<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                </div>
			</div>
        </div>
        <div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="single-post">
					<div class="post-thumbnail">
                        <a href="blog.html"><img src="assets/img/blog/blog1.jpg" alt="blog"></a>
					</div>
					<div class="post-details">
                        <div class="post-author">
							<a href="blog.html"><i class="icofont icofont-user"></i>John</a>
							<a href="blog.html"><i class="icofont icofont-speech-comments"></i>Comments</a>
							<a href="blog.html"><i class="icofont icofont-calendar"></i>21 Feb 2018</a>
                        </div>
                        <h4 class="post-title"><a href="blog.html">Lorem ipsum dolor sit</a></h4>
                        <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad</p>
					</div>
                </div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="single-post">
					<div class="post-thumbnail">
                        <a href="blog.html"><img src="{{asset('assets/img/blog/blog2.jpg')}}" alt="blog"></a>
					</div>
					<div class="post-details">
                        <div class="post-author">
							<a href="blog.html"><i class="icofont icofont-user"></i>John</a>
							<a href="blog.html"><i class="icofont icofont-speech-comments"></i>Comments</a>
							<a href="blog.html"><i class="icofont icofont-calendar"></i>21 Feb 2018</a>
                        </div>
                        <h4 class="post-title"><a href="blog.html">Lorem ipsum dolor sit</a></h4>
                        <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad</p>
					</div>
                </div>
			</div>
			<div class="col-lg-4 d-md-none d-lg-block">
				<div class="single-post">
					<div class="post-thumbnail">
                        <a href="blog.html"><img src="{{asset('assets/img/blog/blog3.jpg')}}" alt="blog"></a>
					</div>
					<div class="post-details">
                        <div class="post-author">
							<a href="blog.html"><i class="icofont icofont-user"></i>John</a>
							<a href="blog.html"><i class="icofont icofont-speech-comments"></i>Comments</a>
							<a href="blog.html"><i class="icofont icofont-calendar"></i>21 Feb 2018</a>
                        </div>
                        <h4 class="post-title"><a href="blog.html">Lorem ipsum dolor sit</a></h4>
                        <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad</p>
					</div>
                </div>
			</div>
        </div>
	</div>
</section> -->
<div class="google-map"></div>
@endsection
