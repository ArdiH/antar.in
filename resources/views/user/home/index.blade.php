@extends('user.layouts.app')
@section('content')
<section class="hero-area" id="home">
	<div class="hero-area-slider">
    	<div class="hero-area-single-slide">
			<div class="container">
                <div class="row">
					<div class="col-lg-7">
                    	<div class="hero-area-content">
							<h1>Kemana Saja Bisa!</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
							<a href="#" class="appao-btn">Google Play</a>
							<a href="#" class="appao-btn">App Store</a>
                        </div>
					</div>
					<div class="col-lg-5">
                        <div class="hand-mockup text-lg-left text-center">
							<img src="{{asset('assets/img/preview.png')}}" alt="Hand Mockup" />
                        </div>
					</div>
                </div>
			</div>
        </div>
        <div class="hero-area-single-slide">
			<div class="container">
                <div class="row">
						<div class="col-lg-7">
                        	<div class="hero-area-content">
								<h1>Aman dan Mudah</h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
								<a href="#" class="appao-btn">Google Play</a>
								<a href="#" class="appao-btn">App Store</a>
                            </div>
						</div>
						<div class="col-lg-5">
                            <div class="hand-mockup text-lg-left text-center">
								<img src="{{asset('assets/img/preview.png')}}" alt="Hand Mockup" />
                            </div>
						</div>
                    </div>
				</div>
            </div>
            <div class="hero-area-single-slide">
				<div class="container">
                    <div class="row">
						<div class="col-lg-7">
                            <div class="hero-area-content">
								<h1>Pertama Di Indonesia</h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
								<a href="#" class="appao-btn">Google Play</a>
								<a href="#" class="appao-btn">App Store</a>
                            </div>
						</div>
						<div class="col-lg-5">
                            <div class="hand-mockup text-lg-left text-center">
								<img src="{{asset('assets/img/preview.png')}}" alt="Hand Mockup" />
                            </div>
						</div>
                    </div>
				</div>
            </div>
		</div>
	</div>
</section>
<section class="about-area ptb-90">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="sec-title">
					<h2>About Antarin<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
					<p>Solusi untuk anda yang susah cari kendaraan untuk keluar kota</p>
                </div>
			</div>
        </div>
        <div class="row">
			<div class="col-lg-4">
				<div class="single-about-box">
					<i class="fas fa-map-marker-alt"></i>
					<h4>Pilih Lokasi Penjemputan</h4>
					<p>Pilih travel berdasarkan lokasi penjemputan</p>
                </div>
			</div>
			<div class="col-lg-4">
				<div class="single-about-box active">
					<i class="fas fa-car"></i>
					<h4>Pilih Travel</h4>
					<p>Pilih travel dengan dengan penawaran terbaik</p>
                </div>
			</div>
			<div class="col-lg-4">
				<div class="single-about-box">
					<i class="fas fa-money"></i>
					<h4>Pembayaran Mudah & Aman</h4>
					<p>Banyak pilihan metode pembayaran yang dapat Anda gunakan</p>
                </div>
			</div>
        </div>
	</div>
</section>
<!-- <section class="feature-area ptb-90" id="feature">
	<div class="container">
        <div class="row flexbox-center">
			<div class="col-lg-4">
                <div class="single-feature-box text-lg-right text-center">
					<ul>
                    	<li>
							<div class="feature-box-info">
                            	<h4>Unlimited Features</h4>
                            	<p>Lorem ipsum dolor amet consectetur adipisicing eiusmod </p>
							</div>
							<div class="feature-box-icon">
                            	<i class="icofont icofont-brush"></i>
							</div>
                        </li>
                        <li>
							<div class="feature-box-info">
                                <h4>Responsive Design</h4>
                                <p>Lorem ipsum dolor amet consectetur adipisicing eiusmod </p>
							</div>
							<div class="feature-box-icon">
                                <i class="icofont icofont-computer"></i>
							</div>
                        </li>
                        <li>
							<div class="feature-box-info">
                                <h4>Well Documented</h4>
                                <p>Lorem ipsum dolor amet consectetur adipisicing eiusmod </p>
							</div>
							<div class="feature-box-icon">
                                <i class="icofont icofont-law-document"></i>
							</div>
                        </li>
                        <li>
							<div class="feature-box-info">
								<h4>Full Free Chat</h4>
								<p>Lorem ipsum dolor amet consectetur adipisicing eiusmod </p>
							</div>
							<div class="feature-box-icon">
								<i class="icofont icofont-heart-beat"></i>
							</div>
                        </li>
					</ul>
                </div>
			</div>
			<div class="col-lg-4">
                <div class="single-feature-box text-center">
					<img src="{{asset('assets/img/feature.png')}}" alt="feature">
                </div>
			</div>
			<div class="col-lg-4">
                    <div class="single-feature-box text-lg-left text-center">
						<ul>
                            <li>
								<div class="feature-box-icon">
                                    <i class="icofont icofont-eye"></i>
								</div>
								<div class="feature-box-info">
                                    <h4>Retina ready</h4>
                                    <p>Lorem ipsum dolor amet consectetur adipisicing eiusmod </p>
								</div>
                            </li>
                            <li>
								<div class="feature-box-icon">
                                	<i class="icofont icofont-sun-alt"></i>
								</div>
								<div class="feature-box-info">
                                    <h4>High Resolution</h4>
                                    <p>Lorem ipsum dolor amet consectetur adipisicing eiusmod </p>
								</div>
                            </li>
                            <li>
								<div class="feature-box-icon">
                                    <i class="icofont icofont-code-alt"></i>
								</div>
								<div class="feature-box-info">
                                    <h4>Clean Codes</h4>
                                    <p>Lorem ipsum dolor amet consectetur adipisicing eiusmod </p>
								</div>
                            </li>
                            <li>
								<div class="feature-box-icon">
                                    <i class="icofont icofont-headphone-alt"></i>
								</div>
								<div class="feature-box-info">
                                    <h4>Helping Supports</h4>
                                    <p>Lorem ipsum dolor amet consectetur adipisicing eiusmod </p>
								</div>
                            </li>
						</ul>
                    </div>
				</div>
            </div>
		</div>
	</div>
</section>
<section class="showcase-area ptb-90">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="sec-title">
					<h2>Showcase<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
                </div>
			</div>
        </div>
        <div class="row flexbox-center">
			<div class="col-lg-6">
            	<div class="single-showcase-box">
					<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi ut aliquip </p>
					<a href="#" class="appao-btn appao-btn2">Read More</a>
                </div>
			</div>
			<div class="col-lg-6">
                <div class="single-showcase-box">
					<img src="{{asset('assets/img/showcase.png')}}" alt="showcase">
                </div>
			</div>
        </div>
        <div class="row flexbox-center">
			<div class="col-lg-6">
                <div class="single-showcase-box">
					<img src="{{asset('assets/img/showcase2.png')}}" alt="showcase">
                </div>
			</div>
			<div class="col-lg-6">
                <div class="single-showcase-box">
					<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi ut aliquip </p>
					<a href="#" class="appao-btn appao-btn2">Read More</a>
                </div>
			</div>
        </div>
	</div>
</section> -->
<section class="video-area ptb-90">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="video-popup">
					<a href="https://www.youtube.com/watch?v=RZXnugbhw_4" 
					><i class="fas fa-play"></i></a>
					<h1>Watch Video Demo</h1>
                </div>
			</div>
        </div>
	</div>
</section>
<section class="screenshots-area ptb-90" id="screenshot">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="sec-title">
					<h2>Screenshot<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                </div>
			</div>
        </div>
        <div class="row">
			<div class="col-lg-12">
                <div class="screenshot-wrap">
					<div class="single-screenshot">
                        <img src="{{asset('assets/img/screenshot/Login.png')}}" alt="screenshot" />
					</div>
					<div class="single-screenshot">
                        <img src="{{asset('assets/img/screenshot/SignUp.png')}}" alt="screenshot" />
					</div>
					<div class="single-screenshot">
                        <img src="{{asset('assets/img/screenshot/Home.png')}}" alt="screenshot" />
					</div>
					<div class="single-screenshot">
                        <img src="{{asset('assets/img/screenshot/screenshot4.jpg')}}" alt="screenshot" />
					</div>
					<div class="single-screenshot">
                        <img src="{{asset('assets/img/screenshot/screenshot5.jpg')}}" alt="screenshot" />
					</div>
                </div>
			</div>
        </div>
	</div>
</section>
<!-- <section class="pricing-area ptb-90" id="pricing">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="sec-title">
					<h2>Our Pricing Plan<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                </div>
			</div>
        </div>
        <div class="row">
			<div class="col-lg-4">
                <div class="single-pricing-box">
					<div class="pricing-top">
                        <h4>Basic</h4>
                        <p>Suitable for Freelancer</p>
					</div>
					<div class="price">
                        <h1><span>$</span>99</h1>
                        <p>Basic</p>
					</div>
					<div class="price-details">
                        <ul>
							<li>Email Marketing</li>
							<li>Email Builder</li>
							<li>Client Testing</li>
							<li>Multiple Email Support</li>
							<li>Email Read Receipent</li>
							<li>2 User Free</li>
                        </ul>
                        <a class="appao-btn" href="#">Order Now</a>
					</div>
                </div>
			</div>
			<div class="col-lg-4">
                <div class="single-pricing-box">
					<div class="pricing-top">
                        <h4>Pro</h4>
                        <p>Suitable for Freelancer</p>
					</div>
					<div class="price">
                        <h1><span>$</span>199</h1>
                        <p>Basic</p>
					</div>
					<div class="price-details">
                        <ul>
							<li>Email Marketing</li>
							<li>Email Builder</li>
							<li>Client Testing</li>
							<li>Multiple Email Support</li>
							<li>Email Read Receipent</li>
							<li>2 User Free</li>
                        </ul>
                        <a class="appao-btn" href="#">Order Now</a>
					</div>
                </div>
			</div>
			<div class="col-lg-4">
                <div class="single-pricing-box">
					<div class="pricing-top">
                        <h4>Ultimate</h4>
                        <p>Suitable for Freelancer</p>
					</div>
					<div class="price">
                        <h1><span>$</span>299</h1>
                        <p>Basic</p>
					</div>
					<div class="price-details">
                        <ul>
							<li>Email Marketing</li>
							<li>Email Builder</li>
							<li>Client Testing</li>
							<li>Multiple Email Support</li>
							<li>Email Read Receipent</li>
							<li>2 User Free</li>
                        </ul>
                        <a class="appao-btn" href="#">Order Now</a>
					</div>
                </div>
			</div>
        </div>
	</div>
</section>
<section class="testimonial-area ptb-90">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="sec-title">
					<h2>Testimonials<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                </div>
			</div>
        </div>
        <div class="row">
			<div class="col-lg-8 offset-lg-2">
                <div class="testimonial-wrap">
					<div class="single-testimonial-box">
                        <div class="author-img">
							<img src="{{asset('assets/img/author/author1.jpg')}}" alt="author" />
                        </div>
                        <h5>Mary Balogh</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi  aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in </p>
                        <div class="author-rating">
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
                        </div>
					</div>
					<div class="single-testimonial-box">
                        <div class="author-img">
							<img src="{{asset('assets/img/author/author2.jpg')}}" alt="author" />
                        </div>
                            <h5>Mary Balogh</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi  aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in </p>
                        <div class="author-rating">
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
                        </div>
					</div>
					<div class="single-testimonial-box">
                        <div class="author-img">
							<img src="{{asset('assets/img/author/author2.jpg')}}" alt="author" />
                        </div>
                            <h5>Mary Balogh</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi  aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in </p>
                        <div class="author-rating">
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
							<i class="icofont icofont-star"></i>
                        </div>
					</div>
                </div>
                <div class="testimonial-thumb">
					<div class="thumb-prev">
                        <div class="author-img">
							<img src="{{asset('assets/img/author/author2.jpg')}}" alt="author" />
                        </div>
					</div>
					<div class="thumb-next">
                        <div class="author-img">
							<img src="{{asset('assets/img/author/author2.jpg')}}" alt="author" />
                        </div>
					</div>
                </div>
			</div>
        </div>
	</div>
</section>
<section class="counter-area ptb-90">
	<div class="container">
        <div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="single-counter-box">
					<i class="icofont icofont-heart-alt"></i>
					<h1><span class="counter">9798</span></h1>
					<p>Happy Client</p>
                </div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="single-counter-box">
					<i class="icofont icofont-protect"></i>
					<h1><span class="counter">9798</span></h1>
					<p>Completed Project</p>
                </div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="single-counter-box">
					<i class="icofont icofont-download-alt"></i>
					<h1><span class="counter">979</span>K</h1>
					<p>Apps Download</p>
                </div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="single-counter-box">
					<i class="icofont icofont-trophy"></i>
					<h1><span class="counter">250</span></h1>
					<p>Our Award</p>
                </div>
			</div>
        </div>
	</div>
</section> -->
<section class="download-area ptb-90">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="sec-title">
					<h2>Download Available<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                </div>
			</div>
        </div>
        <div class="row">
			<div class="col-lg-12">
                <ul>
					<li>
                        <a href="#" class="download-btn flexbox-center">
							<div class="download-btn-icon">
								<i class="fab fa-android"></i>
							</div>
							<div class="download-btn-text">
								<p>Available on</p>
								<h4>Android Store</h4>
							</div>
                        </a>
					</li>
					<li>
                        <a href="#" class="download-btn flexbox-center">
							<div class="download-btn-icon">
                                <i class="fab fa-apple"></i>
							</div>
							<div class="download-btn-text">
                                <p>Available on</p>
                                <h4>Apple Store</h4>
							</div>
                        </a>
					</li>
					<li>
                        <a href="#" class="download-btn flexbox-center">
							<div class="download-btn-icon">
                                 <i class="fab fa-windows"></i>
							</div>
							<div class="download-btn-text">
                                <p>Available on</p>
                                <h4>Windows Store</h4>
							</div>
                        </a>
					</li>
                </ul>
			</div>
        </div>
	</div>
</section>
<section class="team-area ptb-90" id="team">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="sec-title">
					<h2>Meet Our Team<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                </div>
			</div>
        </div>
        <div class="row">
			<div class="col-lg-4 col-sm-6">
				<div class="single-team-member">
					<div class="team-member-img">
                        <img src="{{asset('assets/img/team/hafiz.jpeg')}}" alt="team">
                        <div class="team-member-icon">
							<div class="display-table">
                                <div class="display-tablecell">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
									<a href="#"><i class="fab fa-instagram"></i></a>
									<a href="#"><i class="fab fa-twitter"></i></a>>
                                </div>
							</div>
                        </div>
					</div>
					<div class="team-member-info">
                        <a href="#"><h4>Hafiz</h4></a>
                        <!-- <p>UX Designer</p> -->
					</div>
                </div>
			</div>
			<div class="col-lg-4 col-sm-6">
				<div class="single-team-member">
					<div class="team-member-img">
                        <img src="{{asset('assets/img/team/anas.jpeg')}}" alt="team">
                        <div class="team-member-icon">
							<div class="display-table">
                                <div class="display-tablecell">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
									<a href="#"><i class="fab fa-instagram"></i></a>
									<a href="#"><i class="fab fa-twitter"></i></a>
                                </div>
							</div>
                        </div>
					</div>
					<div class="team-member-info">
                        <a href="#"><h4>Anas</h4></a>
                        <!-- <p>UX Designer</p> -->
					</div>
                </div>
			</div>
			<div class="col-lg-4 col-sm-6">
				<div class="single-team-member">
					<div class="team-member-img">
                        <img src="{{asset('assets/img/team/ihul.jpeg')}}" alt="team">
                        <div class="team-member-icon">
							<div class="display-table">
                                <div class="display-tablecell">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
									<a href="#"><i class="fab fa-instagram"></i></a>
									<a href="#"><i class="fab fa-twitter"></i></a>
                                </div>
							</div>
                        </div>
					</div>
					<div class="team-member-info">
                        <a href="#"><h4>Ihul</h4></a>
                        <!-- <p>UX Designer</p> -->
					</div>
                </div>
			</div>
        </div>
	</div>
</section>
<!-- <section class="blog-area ptb-90" id="blog">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="sec-title">
					<h2>Our Latest Blog<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                </div>
			</div>
        </div>
        <div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="single-post">
					<div class="post-thumbnail">
                        <a href="blog.html"><img src="assets/img/blog/blog1.jpg" alt="blog"></a>
					</div>
					<div class="post-details">
                        <div class="post-author">
							<a href="blog.html"><i class="icofont icofont-user"></i>John</a>
							<a href="blog.html"><i class="icofont icofont-speech-comments"></i>Comments</a>
							<a href="blog.html"><i class="icofont icofont-calendar"></i>21 Feb 2018</a>
                        </div>
                        <h4 class="post-title"><a href="blog.html">Lorem ipsum dolor sit</a></h4>
                        <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad</p>
					</div>
                </div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="single-post">
					<div class="post-thumbnail">
                        <a href="blog.html"><img src="{{asset('assets/img/blog/blog2.jpg')}}" alt="blog"></a>
					</div>
					<div class="post-details">
                        <div class="post-author">
							<a href="blog.html"><i class="icofont icofont-user"></i>John</a>
							<a href="blog.html"><i class="icofont icofont-speech-comments"></i>Comments</a>
							<a href="blog.html"><i class="icofont icofont-calendar"></i>21 Feb 2018</a>
                        </div>
                        <h4 class="post-title"><a href="blog.html">Lorem ipsum dolor sit</a></h4>
                        <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad</p>
					</div>
                </div>
			</div>
			<div class="col-lg-4 d-md-none d-lg-block">
				<div class="single-post">
					<div class="post-thumbnail">
                        <a href="blog.html"><img src="{{asset('assets/img/blog/blog3.jpg')}}" alt="blog"></a>
					</div>
					<div class="post-details">
                        <div class="post-author">
							<a href="blog.html"><i class="icofont icofont-user"></i>John</a>
							<a href="blog.html"><i class="icofont icofont-speech-comments"></i>Comments</a>
							<a href="blog.html"><i class="icofont icofont-calendar"></i>21 Feb 2018</a>
                        </div>
                        <h4 class="post-title"><a href="blog.html">Lorem ipsum dolor sit</a></h4>
                        <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad</p>
					</div>
                </div>
			</div>
        </div>
	</div>
</section> -->
<div class="google-map"></div>
@endsection
