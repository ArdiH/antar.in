@extends('user.layouts.app')
@section('content')
<section class="hero-area" id="home">
    <div class="container">
        <div class="row">
		    <div class="col-lg-7">
                <div class="hero-area-content">
				    <h1>Pesan Sekarang !!</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
					<a href="#" class="appao-btn">Google Play</a>
					<a href="#" class="appao-btn">App Store</a>
                </div>
			</div>
			<div class="col-lg-5">
                <div class="hand-mockup text-lg-left text-center">
					<img src="{{asset('assets/img/preview.png')}}" alt="Hand Mockup" />
                </div>
			</div>
        </div>
	</div>
</section>
<section class="about-area ptb-90">
	<div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class="sec-title">
					<h2>Pesan Travel<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
					<p>Silahkan isi ketentuan di bawah ini !!</p>
                </div>
			</div>
        </div>
        <div class="row">
            <div class="col">
                <select class="form-control form-control-lg">
                    <option>Pilih Lokasi Penjemputan</option>
                </select>
            </div>
            <div class="col">
                <select class="form-control form-control-lg">
                    <option>Pilih Tujuan Akhir</option>
                </select>
            </div>
            <div class="col">
                <button type="button" class="btn btn-primary btn-lg btn-block">Submit</button>
            </div>
        </div>
	</div>
</section>
<div class="google-map"></div>
@endsection
